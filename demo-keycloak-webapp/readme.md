### prepare karaf
- download karaf
https://www.apache.org/dyn/closer.lua/karaf/4.3.5/apache-karaf-4.3.5.zip

- download Keycloak JBoss Fuse Adapters
https://github.com/keycloak/keycloak/releases/download/16.1.0/keycloak-oidc-fuse-adapter-16.1.0.zip

- extract system folder within adapters zip into karaf system folder

- start karaf
```
./bin/karaf
```
- type in Karaf
```
repo-add mvn:org.keycloak/keycloak-osgi-features/16.1.0/xml/features
feature:install keycloak-servlet-filter-adapter
feature:install war
```

### prepare keycloak
- start keycloak in Docker
```
docker run -d -p 18181:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin --name keycloak jboss/keycloak
```
or on local installation with port 18181

- create a Client in Keycloak with the settings below
```
name: karaf
access type: confidential
admin Url: http://localhost:8181/sample/keycloak
redirect_uri: http://localhost:8181/*
```
### prepare application
- copy keycloak.json from the installation tab into **src/main/webapp/WEB-INF/keycloak.json**

- Build artifact: type following command from root directory of application project
```
mvn clean install
```

- Deploy artifact into karaf
```
bundle:install -s webbundle:mvn:org.example/demo-keycloak-webapp/1.0-SNAPSHOT/war?Web-ContextPath=sample
```
### check application
The application ist now on the url http://localhost:8181/sample available.

Mainpage http://localhost:8181/sample is unsecured like login page in a web application

All Paths are secured, that beginning the url http://localhost:8181/sample/protected for example
http://localhost:8181/sample/protected/protected.jsp


